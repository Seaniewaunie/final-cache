# $1 is the path to your files
# $2 is the name of the file

cdslib=/afs/umbc.edu/users/s/g/sgraff2/home/CMPE315/cds.lib
hdlvar=/afs/umbc.edu/users/s/g/sgraff2/home/CMPE315/hdl.var
vhdl='/afs/umbc.edu/users/s/g/sgraff2/home/CMPE315/run_ncvhdl.bash'
vhdl_flags="$4 -messages -linedebug -cdslib $cdslib -hdlvar $hdlvar -smartorder"
vhd_file="$1$2.vhd"
vhd_test="$1$2_test$3.vhd"

elab='/afs/umbc.edu/users/s/g/sgraff2/home/CMPE315/run_ncelab.bash'
elab_flags="$4 -messages -access rwc -cdslib $cdslib -hdlvar $hdlvar"
elab_file="$2_test$3"

sim='/afs/umbc.edu/users/s/g/sgraff2/home/CMPE315/run_ncsim.bash'
sim_flags="-input $1ncsim.run -messages -cdslib $cdslib -hdlvar $hdlvar"
sim_file="$2_test$3"

# Compiling inverter.vhd
echo $vhdl $vhdl_flags $vhd_file
$vhdl $vhdl_flags $vhd_file
# Compiling the inverter_test.vhd
echo $vhdl $vhdl_flags $vhd_test
$vhdl $vhdl_flags $vhd_test

# Elaborating vhdl
#echo $elab $elab_flags $elab_file
$elab $elab_flags $elab_file
# Simulating vhdl
echo $sim $sim_flags $sim_file
$sim $sim_flags $sim_file

echo "--------in file------------"
cat $1$2_in.txt
echo "--------out file------------"
cat $1$2_out.txt
