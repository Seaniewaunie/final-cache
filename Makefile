# VHDL files
SRCDIR = src
FILES = $(SRCDIR)/inverter.vhd $(SRCDIR)/nand2.vhd \
	$(SRCDIR)/nor2.vhd  $(SRCDIR)/dff.vhd \
	$(SRCDIR)/latch.vhd $(SRCDIR)/and2.vhd \
	$(SRCDIR)/and3.vhd $(SRCDIR)/xnor2.vhd \
	$(SRCDIR)/xor2.vhd $(SRCDIR)/or2.vhd \
	$(SRCDIR)/register8.vhd $(SRCDIR)/tx.vhd \
	$(SRCDIR)/tx8.vhd $(SRCDIR)/colEn.vhd \
	$(SRCDIR)/rowEn.vhd $(SRCDIR)/counter_5bit.vhd \
	$(SRCDIR)/memCell.vhd $(SRCDIR)/valid_tag.vhd \
	$(SRCDIR)/memBlock.vhd \
	$(SRCDIR)/hitmiss.vhd $(SRCDIR)/mux_2to1.vhd \
	$(SRCDIR)/stateMachine.vhd $(SRCDIR)/cache.vhd \
	$(SRCDIR)/chip.vhd

# Testbench
TESTDIR = test
TESTFILES = 	$(TESTDIR)/*.vhd

SUFFIX = _out# Suffix of files created using -e option
MODULE = $(TEST)# Show wave of this module
STOPTIME = 360ns

# Run
RUN_FLAGS = --stop-time=$(STOPTIME) --vcd=$(MODULE).vcd

# VHDL command
cdslib=../cds.lib
hdlvar=../hdl.var
VHDL_CMD = ../run_ncvhdl.bash
VHDL_FLAGS =" -messages -linedebug -cdslib $(cdslib) -hdlvar $(hdlvar) -smartorder"

elab=../run_ncelab.bash
elab_flags="-messages -access rwc -cdslib $(cdslib) -hdlvar $(hdlvar)"
sim=../run_ncsim.bash
sim_flags="-messages -cdslib $(cdslib) -hdlvar $(hdlvar)"
# -input $(TESTDIR)/$(MODULE)_ncsim.run
all: | vhdl-compile vhdl-elaborate vhdl-simulate 
#show

vhdl-compile: $(FILES) $(TESTFILES)
	$(VHDL_CMD) $(VHDL_FLAGS) $(FILES) $(TESTFILES)
clean:
	rm *.o work-obj93.cf *.vcd *$(SUFFIX)

vhdl-elaborate:
	$(elab) $(elab_flags) $(MODULE)

vhdl-simulate:
	$(sim) $(INPUT) $(sim_flags) $(MODULE)

show:
	$(VHDL_CMD) -r $(MODULE)$(SUFFIX) $(RUN_FLAGS)
	gtkwave $(MODULE).vcd
