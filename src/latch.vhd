--
-- Entity: and3
-- Architecture : structural
-- Author: sgraff2
-- Created On: 11/04/2017
--
library STD;
library IEEE;                      
use IEEE.std_logic_1164.all;       

entity latch is                      
  port (          
        clk : in  std_logic;
        d   : in  std_logic;
        q   : out std_logic;
        qbar: out std_logic); 
end latch;                          

architecture structural of latch is 

 
  
begin
 	-- when clk is 1, Q gets D otherwise it's just Q
	-- when clk is 1, Qbar gets not D, otherwise its just Qbar
	output: process (d,clk)                  
	begin                           
	if clk = '1' then 
	    q <= d;
	    qbar <= not d ;
	end if; 
	end process output; 
                             
end structural;  
