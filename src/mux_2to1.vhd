-- Entity: mux_2to1
-- Architecture: structural
-- Author: mousta1
-- Date: 11/08/2017

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity mux_2to1 is
    port(
    sel	:	in std_logic;
    in0  :	in std_logic_vector(1 downto 0);
    in1  :	in std_logic_vector(1 downto 0);
    output:	out std_logic_vector(1 downto 0));
end mux_2to1;   

architecture structural of mux_2to1 is

component inverter
  port (
    input	: in std_logic;
    output	: out std_logic);
end component;

component and2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

component or2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

for invertSelect: inverter use entity work.inverter(structural);
for and2_1, and2_2, and2_3, and2_4: and2 use entity work.and2(structural);
for or2_1, or2_2: or2 use entity work.or2(structural);

signal seln, seln_in00, sel_in10, seln_in01, sel_in11: std_logic;


begin

	invertSelect : inverter port map (sel, seln);

	and2_1 : and2 port map (seln, in0(0), seln_in00);
	and2_2 : and2 port map (sel, in1(0), sel_in10);
	or2_1  : or2  port map (seln_in00, sel_in10, output(0));

	and2_3 : and2 port map (seln, in0(1), seln_in01);
	and2_4 : and2 port map (sel, in1(1), sel_in11);
	or2_2  : or2  port map (seln_in01, sel_in11, output(1));

end structural;
