-- Entity: memBlock
-- Architecture: structural
-- Author: mousta1
-- Date: 11/09/2017

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity memBlock is
  port(
    inputData   : in std_logic_vector(7 downto 0);
    blockEn     : in std_logic;
    blockEnn    : in std_logic;
    byteEn      : in std_logic_vector(3 downto 0);
    inputTag    : in std_logic_vector(2 downto 0);
    rstn        : in std_logic;
    rst		: in std_logic;
    clk		: in std_logic;
    wr    	: in std_logic;
    valid       : out std_logic;
    outputTag      : out std_logic_vector(2 downto 0);
    outputData0  : out std_logic_vector(7 downto 0);    
    outputData1  : out std_logic_vector(7 downto 0);
    outputData2  : out std_logic_vector(7 downto 0);
    outputData3  : out std_logic_vector(7 downto 0));
end memBlock;

architecture structural of memBlock is

component valid_tag
  port(
    blockEn     : in std_logic;
    blockEnn    : in std_logic;
    inputTag    : in std_logic_vector(2 downto 0);
    rstn        : in std_logic;
    rst		: in std_logic;
    clk		: in std_logic;
    wr    	: in std_logic;
    valid       : out std_logic;
    outputTag      : out std_logic_vector(2 downto 0));
end component;

component memCell
  port (
    input   : in  std_logic_vector(7 downto 0);	
    rowEn	: in  std_logic;		
    colEn	: in  std_logic;	
    wr   	: in  std_logic;				
    output  : out std_logic_vector(7 downto 0));
end component;

for vt_1 : valid_tag use entity work.valid_tag(structural);

for mem_0, mem_1, mem_2, mem_3: memCell use entity work.memCell(structural);

begin

    vt_1 : valid_tag port map (blockEn, blockEnn, inputTag, rstn, rst, clk, wr, valid, outputTag);
    
    -- the byte select determines which memcell is being used. 
    mem_0 : memCell port map (inputData, blockEn, byteEn(0), wr, outputData0);
    mem_1 : memCell port map (inputData, blockEn, byteEn(1), wr, outputData1);
    mem_2 : memCell port map (inputData, blockEn, byteEn(2), wr, outputData2);
    mem_3 : memCell port map (inputData, blockEn, byteEn(3), wr, outputData3);
                
end structural;
