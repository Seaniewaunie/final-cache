--
-- Entity: chip
-- Architecture : structural
-- Author: sgraff2
-- Created on 11/16/2017
--

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity chip is
    port(
        cpu_add    : in std_logic_vector(7 downto 0);
        cpu_data   : inout std_logic_vector(7 downto 0);
        cpu_rd_wrn : in std_logic;
        start      : in std_logic;
        clk        : in std_logic;
        reset      : in std_logic;
        mem_data   : in std_logic_vector(7 downto 0);
        Vdd        : in std_logic;
        Gnd        : in std_logic;
        busy       : out std_logic;
        mem_en      : out std_logic;
        mem_add    : out std_logic_vector(7 downto 0));
end chip;

architecture structural of chip is

-- inverter
component inverter

  port (
    input    : in  std_logic;
    output   : out std_logic);
end component;

-- and2
component and2

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

-- mux_2to1
component mux_2to1
    port(
        sel     : in std_logic;
        in0     : in std_logic_vector(1 downto 0);
        in1     : in std_logic_vector(1 downto 0);
        output  : out std_logic_vector(1 downto 0));
end component;   

-- hitmiss
component hitmiss
    port(
        in1    : in  std_logic_vector(2 downto 0);
        in2    : in  std_logic_vector(2 downto 0);
        valid  : in  std_logic;
        hit    : out std_logic );
end component;

-- tx
component tx                    
  port ( sel   : in std_logic;
         selnot: in std_logic;
         input : in std_logic;
         output:out std_logic);
end component;

-- tx8
component tx8                     
  port ( sel   : in std_logic;
         selnot: in std_logic;
         input : in std_logic_vector(7 downto 0);
         output:out std_logic_vector(7 downto 0));
end component;                          

-- dff
component dff                      
  port ( d   : in  std_logic;
         clk : in  std_logic;
         q   : out std_logic;
         qbar: out std_logic); 
end component;                          

-- register8
component register8                      
  port ( clk    : in  std_logic;
         input  : in std_logic_vector(7 downto 0);
         output : out std_logic_vector(7 downto 0)); 
end component;                          

-- cache
component cache
port (
    inputData      : in  std_logic_vector(7 downto 0);
    offBlock    : in  std_logic_vector(2 downto 0);
    offByte     : in  std_logic_vector(1 downto 0);
    tagIn       : in  std_logic_vector(2 downto 0);
    reset		: in  std_logic;
    clk		: in  std_logic;
    write	: in  std_logic;
    valid	: out std_logic;
    tagOut	: out std_logic_vector(2 downto 0);
    outputData	: out std_logic_vector(7 downto 0));	
end component;

-- stateMachine
component stateMachine
    port(
        clk     : in std_logic;
        start   : in std_logic;
        hitMiss : in std_logic;
        read    : in std_logic;
        write   : in std_logic;
        memEn   : out std_logic;
        cacheAd : out std_logic_vector(1 downto 0);
        cacheWr : out std_logic;
        CPUoutEn: out std_logic;
        busy    : out std_logic);
end component;


for inv_DataOutput, inv_miss, inv_start, inv_MemEn, inv_buff1: inverter use entity work.inverter(structural);
for and_DataClk, and_ReadMiss: and2 use entity work.and2(structural);
for mux_byte: mux_2to1 use entity work.mux_2to1(structural);
for hitMiss0: hitmiss use entity work.hitmiss(structural);
for tx8_inputData, tx8_memData, tx8_DataOutput, tx8_memOutput: tx8 use entity work.tx8(structural);
for dff_readwrite, dff_hit: dff use entity work.dff(structural);
for reg8_address, reg8_data: register8 use entity work.register8(structural);
for cache1: cache use entity work.cache(structural);
for stateMachine1: stateMachine use entity work.stateMachine(structural);
for tx_hitd, tx_hit: tx use entity work.tx(structural);

signal dataClk, read, write, hitt, hit, miss, rdMiss, cacheWr, outputEn, outputEnn, valid: std_logic;
signal tempVdd, tempGnd, tempVddn, tempGndn, hitd, startn: std_logic;
signal address, dataIn, dataInt, dataOutt: std_logic_vector (7 downto 0);
signal tagOut: std_logic_vector (2 downto 0);
signal RMByte, cacheByteOff: std_logic_vector(1 downto 0);
signal memEnable, memEnn : std_logic;

begin
    -- we will need to and start and clk to start once clk is on a rising edge
    and_DataClk : and2 port map (start, clk, dataClk);

    -- store the cpu instruction for rd/wr 
    dff_readwrite : dff  port map (cpu_rd_wrn, dataClk, read, write);
    
    -- store the address and data from the cpu
    reg8_address : register8 port map (dataClk, cpu_add, address);
    reg8_data : register8 port map (dataClk, cpu_data , dataInt);

    -- transmit the data for write from cpu 
    tx8_inputData : tx8 port map (write, read, dataInt, dataIn); 
    -- transmit the data for read from memory
    tx8_memData : tx8 port map (read, write, mem_data, dataIn);

    -- calculate if it was a hit by sending the tag
    hitMiss0 : hitmiss port map (address (7 downto 5), tagOut, valid, hitt);
    -- store the hit value until start goes low, then transmit it to hitd
    dff_hit : dff port map (hitt, start, hitd,  open);

    -- invert start for use in a transmission gate
    inv_start : inverter port map (start, startn);
    -- transmit the hit data when start is low
    tx_hitd : tx port map (startn, start, hitd, hit);

    -- transmit the hit value and invert it to get the miss signal
    tx_hit : tx port map (start, startn, hitt, hit);
    inv_miss : inverter port map (hit, miss);	
    
    -- send all the signals to the statemachine
    stateMachine1 : stateMachine port map (clk, start, hit, read, write, memEnable, RMByte, cacheWr, outputEn, busy);

    -- invert the output enable signal for transmission
    inv_DataOutput : inverter port map (outputEn, outputEnn);
    -- transmit the data when output enable is high from the state machine
    tx8_DataOutput : tx8 port map (outputEn, outputEnn, dataOutt, cpu_data);

    -- if it's a read miss, select the byte offset to send to the cache
    and_ReadMiss : and2 port map (read, miss, rdMiss);
    mux_byte : mux_2to1 port map (rdMiss, address(1 downto 0), RMByte, cacheByteOff); 

    -- send all the signals to the cache
    cache1 : cache port map (dataIn, address(4 downto 2), cacheByteOff, address(7 downto 5), reset, clk, cacheWr, valid, tagOut, dataOutt);

    -- transmit the memory address
    inv_MemEn : inverter port map(memEnable, memEnn);
    tx8_memOutput : tx8 port map(memEnable, memEnn, address, mem_add);
    inv_buff1 : inverter port map (memEnn, mem_en);

end structural;
