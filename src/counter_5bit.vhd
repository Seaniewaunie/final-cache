-- Entity: counter_5bit
-- Architecture: structural
-- Author: mousta1
-- Due: 11/22/2017

library STD;
library IEEE;                      
use IEEE.std_logic_1164.all;       

entity counter_5bit is                      
  port ( 
     countEn 	 : in  std_logic;
     reset	 : in  std_logic;
     Clk  	 : in std_logic;
     output      : out  std_logic_vector(4 downto 0);
     outputn     : out std_logic_vector(4 downto 0)); 
end counter_5bit;                          

architecture structural of counter_5bit is

component inverter
  port ( input	: in std_logic;
	 output	: out std_logic);
end component;

component and2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

component xor2
  port (
    input1   : in std_logic;
    input2   : in std_logic;
    output   : out std_logic);
end component;

component dff
  port ( d   : in  std_logic;
         clk : in  std_logic;
         q   : out std_logic;
         qbar: out std_logic); 
end component;


for inv_1 : inverter use entity work.inverter(structural);
for and2_c1, and2_c2, and2_c3, and2_c4 : and2 use entity work.and2(structural);
for and2_r1, and2_r2, and2_r3, and2_r4, and2_r5 : and2 use entity work.and2(structural);
for xor2_c1, xor2_c2, xor2_c3, xor2_c4, xor2_c5	: xor2 use entity work.xor2(structural);
for dff_1, dff_2, dff_3, dff_4, dff_5 : dff use entity work.dff(structural);


for bufinv_0, bufinv_1, bufinv_2, bufinv_3, bufinv_4, bufinv_5, bufinv_6, bufinv_7, bufinv_8, bufinv_9 : inverter use entity work.inverter(structural);

signal inputD : std_logic_vector(4 downto 0);
signal checkResetD : std_logic_vector(4 downto 0);
signal currentCount1, currentCount2, currentCount3, currentCount4 : std_logic;
signal resetn : std_logic;
signal outputemp : std_logic_vector(4 downto 0);
signal buff : std_logic_vector(4 downto 0);

begin

	-- Do the logic to determine if a count is supposed to happen
	and2_c1 : and2 port map ( countEn, outputemp(0), currentCount1 );
	and2_c2 : and2 port map ( currentCount1, outputemp(1), currentCount2 );
	and2_c3 : and2 port map ( currentCount2, outputemp(2), currentCount3 );
	and2_c4 : and2 port map ( currentCount3, outputemp(3), currentCount4 );

	xor2_c1 : xor2 port map ( countEn, outputemp(0), inputD(0) );
	xor2_c2 : xor2 port map ( currentCount1, outputemp(1), inputD(1) );
	xor2_c3 : xor2 port map ( currentCount2, outputemp(2), inputD(2) );
	xor2_c4 : xor2 port map ( currentCount3, outputemp(3), inputD(3) );
	xor2_c5 : xor2 port map ( currentCount4, outputemp(4), inputD(4) );

	-- Here we determine if the counter needs to be reset
	inv_1 : inverter port map ( reset, resetn );

	and2_r1 : and2 port map ( inputD(0), resetn, checkResetD(0) );
	and2_r2 : and2 port map ( inputD(1), resetn, checkResetD(1) );
	and2_r3 : and2 port map ( inputD(2), resetn, checkResetD(2) );
	and2_r4 : and2 port map ( inputD(3), resetn, checkResetD(3) );
	and2_r5 : and2 port map ( inputD(4), resetn, checkResetD(4) );

	-- the flip flops will hold the output
	dff_1 : dff port map ( checkResetD(0), Clk, outputemp(0), outputn(0) );
	dff_2 : dff port map ( checkResetD(1), Clk, outputemp(1), outputn(1) );
	dff_3 : dff port map ( checkResetD(2), Clk, outputemp(2), outputn(2) );
	dff_4 : dff port map ( checkResetD(3), Clk, outputemp(3), outputn(3) );
	dff_5 : dff port map ( checkResetD(4), Clk, outputemp(4), outputn(4) );

	-- The output needs to be buffered with inverters since we dont have a 5 bit tx
	bufinv_0 : inverter port map (outputemp(0), buff(0));
	bufinv_1 : inverter port map (outputemp(1), buff(1));
	bufinv_2 : inverter port map (outputemp(2), buff(2));
	bufinv_3 : inverter port map (outputemp(3), buff(3));
	bufinv_4 : inverter port map (outputemp(4), buff(4));

	bufinv_5 : inverter port map (buff(0), output(0));
	bufinv_6 : inverter port map (buff(1), output(1));
	bufinv_7 : inverter port map (buff(2), output(2));
	bufinv_8 : inverter port map (buff(3), output(3));
	bufinv_9 : inverter port map (buff(4), output(4));

end structural;
