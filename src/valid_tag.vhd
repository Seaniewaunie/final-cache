-- Entity: valid_tag
-- Architecture: structural
-- Author: mousta1
-- Date: 11/09/2017

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity valid_tag is
  port(
    blockEn     : in std_logic;
    blockEnn    : in std_logic;
    inputTag    : in std_logic_vector(2 downto 0);
    rstn        : in std_logic;
    rst		: in std_logic;
    clk		: in std_logic;
    wr    	: in std_logic;
    valid       : out std_logic;
    outputTag      : out std_logic_vector(2 downto 0));
end valid_tag;

architecture structural of valid_tag is

component and2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

component or2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;
  
component latch                 
  port (          
        clk : in  std_logic;
        d   : in  std_logic;
        q   : out std_logic;
        qbar: out std_logic); 
end component;                          

component tx	
  port(
     sel	: in std_logic;
     selnot	: in std_logic;
     input	: in std_logic;
     output	: out std_logic);
end component;
 


for and2valEn 	: and2 use entity work.and2(structural);
for or2valEn	: or2 use entity work.or2(structural);
for rstandclk	: and2 use entity work.and2(structural);

for txval, txtag_0, txtag_1, txtag_2	: tx use entity work.tx(structural);

for latchtag_0	: latch use entity work.latch(structural);
for latchtag_1	: latch use entity work.latch(structural);
for latchtag_2	: latch use entity work.latch(structural);
for latchval 	: latch use entity work.latch(structural);

signal rstandclk1, valEn, validt, wrEn : std_logic;
signal valTagOut : std_logic_vector(2 downto 0);


begin

    -- enable valid and tag writing
    and2valEn   : and2 port map (wr, blockEn, wrEn);
    rstandclk   : and2 port map (rst, clk, rstandclk1);
    or2valEn   : or2 port map (wr, rstandclk1, valEn);
    
    -- valid bit
    latchval    : latch	port map (valEn, rstn, validt, open);
    txval       : tx port map (blockEn, blockEnn, validt, valid );

    -- tag
    latchtag_0  : latch	port map (valEn, inputTag(0), valTagOut(0), open);
    latchtag_1	: latch	port map (valEn, inputTag(1), valTagOut(1), open);
    latchtag_2	: latch	port map (valEn, inputTag(2), valTagOut(2), open);

    txtag_0     : tx port map (blockEn, blockEnn, valTagOut(0), outputTag(0));
    txtag_1     : tx port map (blockEn, blockEnn, valTagOut(1), outputTag(1));
    txtag_2     : tx port map (blockEn, blockEnn, valTagOut(2), outputTag(2));
                       
end structural;
