-- Entity: tx8
-- Architecture : structural
-- Author: sgraff2
-- Date: 11/03/2017
--
  
library STD;
library IEEE;                      
use IEEE.std_logic_1164.all;       

entity tx8 is                      
  port ( sel   : in std_logic;
         selnot: in std_logic;
         input : in std_logic_vector(7 downto 0);
         output:out std_logic_vector(7 downto 0));
end tx8;                          

architecture structural of tx8 is 

component tx
    port ( sel   : in std_logic;
           selnot: in std_logic;
           input : in std_logic;
           output:out std_logic);
end component;

for tx0, tx1, tx2, tx3, tx4, tx5, tx6, tx7: tx use entity work.tx(structural);

begin

    tx0:    tx port map( sel, selnot, input(0), output(0) );
    tx1:    tx port map( sel, selnot, input(1), output(1) );
    tx2:    tx port map( sel, selnot, input(2), output(2) );
    tx3:    tx port map( sel, selnot, input(3), output(3) );
    tx4:    tx port map( sel, selnot, input(4), output(4) );
    tx5:    tx port map( sel, selnot, input(5), output(5) );
    tx6:    tx port map( sel, selnot, input(6), output(6) );
    tx7:    tx port map( sel, selnot, input(7), output(7) );

 
end structural;
