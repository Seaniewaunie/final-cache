-- Entity: colEn
-- Architecture: structural
-- Author: mousta1
-- Date: 11/08/2017

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity colEn is

  port (
    sel	        : in  std_logic_vector(1 downto 0);
    col0	: out  std_logic;
    col1        : out  std_logic;
    col2        : out  std_logic;
    col3	: out  std_logic);
end colEn;

architecture structural of colEn is


component and2
  port (
    input1      : in  std_logic;
    input2      : in  std_logic;
    output      : out std_logic);
end component;

component inverter
  port (
    input	: in std_logic;
    output	: out std_logic);
end component;

for inv_1, inv_0: inverter use entity work.inverter(structural);
for and2_1,and2_2,and2_3,and2_4: and2 use entity work.and2(structural);


signal in1n, in0n: std_logic;


begin
    -- simply a decoder with two selects
      inv_1: inverter port map (sel(1), in1n);
      inv_0: inverter port map (sel(0), in0n);
      
      and2_1: and2 port map (in1n,   in0n,   col0);
      and2_2: and2 port map (in1n,   sel(0), col1);
      and2_3: and2 port map (sel(1), in0n,   col2);
      and2_4: and2 port map (sel(1), sel(0), col3);

end structural;
