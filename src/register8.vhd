--
-- Entity: register8 
-- Architecture : structural
-- Author: sgraff2
-- Date: 11/03/2017
--

library STD;
library IEEE;                      
use IEEE.std_logic_1164.all;       

entity register8 is                      
  port ( clk    : in  std_logic;
         input  : in std_logic_vector(7 downto 0);
         output : out std_logic_vector(7 downto 0)); 
end register8;                          

architecture structural of register8 is 


component dff                     
  port ( d   : in  std_logic;
         clk : in  std_logic;
         q   : out std_logic;
         qbar: out std_logic); 
end component;                          

for dff0, dff1, dff2, dff3, dff4, dff5, dff6, dff7: dff use entity work.dff(structural);
  
begin

    dff0: dff port map( input(0), clk, output(0), open );
    dff1: dff port map( input(1), clk, output(1), open );
    dff2: dff port map( input(2), clk, output(2), open );
    dff3: dff port map( input(3), clk, output(3), open );
    dff4: dff port map( input(4), clk, output(4), open );
    dff5: dff port map( input(5), clk, output(5), open );
    dff6: dff port map( input(6), clk, output(6), open );
    dff7: dff port map( input(7), clk, output(7), open );
                                 
end structural;  


