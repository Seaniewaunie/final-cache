--
-- Entity: stateMachine
-- Architecture : structural
-- Author: sgraff2
-- Created On: 11/11/2017
--

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity stateMachine is
    port(
        clk     : in std_logic;
        start   : in std_logic;
        hitMiss : in std_logic;
        read    : in std_logic;
        write   : in std_logic;
        memEn   : out std_logic;
        cacheAd : out std_logic_vector(1 downto 0);
        cacheWr : out std_logic;
        CPUoutEn: out std_logic;
        busy    : out std_logic);
end stateMachine;

architecture structural of stateMachine is

-- inverter
component inverter

  port (
    input    : in  std_logic;
    output   : out std_logic);
end component;

-- and2
component and2

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

-- and3
component and3

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    input3   : in  std_logic;
    output   : out std_logic);
end component;

-- or2
component or2
	port(
		input1	: in std_logic;
		input2	: in std_logic;
		output	: out std_logic);
end component;

-- counter_5bit
component counter_5bit
  port ( 
     countEn 	 : in  std_logic;
     reset	 : in  std_logic;
     Clk  	 : in std_logic;
     output  : out  std_logic_vector(4 downto 0);
     outputn : out std_logic_vector(4 downto 0)); 
end component;                          

-- latch
component latch                  
  port (          
        clk : in  std_logic;
        d   : in  std_logic;
        q   : out std_logic;
        qbar: out std_logic); 
end component;                          

-- dff
component dff
  port ( d   : in  std_logic;
         clk : in  std_logic;
         q   : out std_logic;
         qbar: out std_logic); 
end component;                          


for inv_miss, inv_t18, inv_start: inverter use entity work.inverter(structural);

for and_readAndMiss, and_b1, and_RMandTime18n: and2 use entity work.and2(structural);
for and_memEn: and2 use entity work.and2(structural);
for and_b00, and_b10, and_b11, and_startAndClk: and2 use entity work.and2(structural);

for and_t20, and_t00, and_t10, and_t161, and_t181, and_cacheAndWrite2: and3 use entity work.and3(structural);
for and_t120, and_t121, and_t160, and_cpuSignals, and_b03, and_cache2: and3 use entity work.and3(structural);

for or_time0OrWrite1, or_busy, or_cache3: or2 use entity work.or2(structural);
for orb01, orb02, orb12, orb13, or_busyOrClock: or2 use entity work.or2(structural);

for counter0: counter_5bit use entity work.counter_5bit(structural);

for latch_rm:	latch use entity work.latch(structural);

for dff_cpuclk, dff_busyclk:	dff use entity work.dff(structural);

for inv_busy1, inv_invbusy2 : inverter use entity work.inverter(structural);

signal miss, RH, RM, RMt, clkn, WH, RM18, RH1, write1, time18n, RMtime18n, busytime0, CPUoutn: std_logic;
signal count011, c4nc3, count01x11, count011c0n, busyt, CPUoutEnt: std_logic;
signal count000, T0, T1, T2, T16, T18: std_logic;
signal T12, count100, b00, b01, b10, b11, b12, startn, CPUoutEntt: std_logic;
signal cpu1clk, stClk, term1, term2, cacheWrt1, cacheWrt2: std_logic;
signal outputT: std_logic_vector (7 downto 0);
signal count, countn: std_logic_vector(4 downto 0);
signal busybuf : std_logic;

begin
    inv_miss : inverter	port map (hitMiss, miss);
    
    and_readAndMiss : and2 port map (read, miss, RMt);
    
    -- latch onto a RM
    inv_start : inverter port map (start, startn);
    latch_rm : latch port map (start, RMt, RM, open);


    -- start the state counter
    counter0 : counter_5bit port map (busyt, start, clk, count, countn);

    -- get the different time signals for the wait periods.

    --T0 is when all the counter bits a low
    and_t20 : and3 port map (countn(4), countn(3), countn(2), count000);
    and_t00 : and3 port map (countn(1), countn(0), count000, T0);

    --T1 is when just the LSB of the counter bits is high
    and_t10 : and3 port map (countn(1), count(0), count000, T1);

    --T12 is 01100 on the counter bits
    and_t120 : and3 port map (countn(4), count(3), count(2), count011);
    and_t121 : and3 port map (countn(1), countn(0), count011, T12);

    --T16 is 10000
    and_t160 : and3 port map (count(4), countn(3), countn(2), count100);
    and_t161 : and3 port map (count100, countn(1), countn(0), T16);

    --T18 is 10010
    and_t181 : and3 port map (count(1), countn(0), count100, T18);

    -- notify Busy signal
    and_b1 : and2 port map (write, T1, write1);
    inv_t18 : inverter	port map (T18, time18n);
    and_RMandTime18n : and2 port map (time18n, RM, RMtime18n);
    or_time0OrWrite1 : or2 port map (T0, write1, busytime0);
    or_busy : or2 port map (busytime0, RMtime18n, busyt);

    -- get CPU output enabled
    and_startAndClk : and2 port map (start, clk, stClk);
    or_busyOrClock : or2 port map (busyt, stClk, cpu1clk);
    dff_cpuclk : dff port map (startn, cpu1clk, term1, open);
    dff_busyclk	: dff port map (busyt, clk, term2, open);
    and_cpuSignals : and3 port map (term1, term2, read, CPUoutEn);

    -- set up the busy signal with a buffer
    inv_busy1 : inverter port map (busyt, busybuf);
    inv_invbusy2 : inverter port map (busybuf, busy );

    -- signal for cache write enable
    and_cacheAndWrite2 : and3 port map (count(3), count(0), clk, cacheWrt1);
    and_cache2 : and3 port map (T1, clk, write, cacheWrt2);
    or_cache3 : or2 port map (cacheWrt1, cacheWrt2, cacheWr);

    -- get the byte address for RM
    and_b03 : and3 port map (countn(4), count(3), count(0), count01x11);
    and_b00 : and2 port map (count(1), count01x11, b00);
    orb01 : or2 port map (b00, T12, b01);
    orb02 : or2 port map (b01, T16, cacheAd(0));


    and_b10 : and2 port map (count(0), count011, b10);
    and_b11 : and2 port map (count(1), count011, b11);
    orb12 : or2 port map (b10, b11, b12);
    orb13 : or2 port map (b12, T16, cacheAd(1));

    -- enable the memory adress
    and_memEn :and2 port map (RM, T1, memEn);

end structural;
