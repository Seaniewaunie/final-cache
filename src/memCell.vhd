-- Entity: memcell
-- Architecture: structural
-- Author: mousta1
-- Date: 11/09/2017

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity memCell is

  port (
    input   : in  std_logic_vector(7 downto 0);	
    rowEn	: in  std_logic; 
    colEn	: in  std_logic; 
    wr   	: in  std_logic;				
    output  : out std_logic_vector(7 downto 0));

end memCell;

architecture structural of memCell is


component inverter
  port (
    input	: in std_logic;
    output	: out std_logic);
end component;

component nand2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

component tx
  port(
     sel	: in std_logic;
     selnot	: in std_logic;
     input	: in std_logic;
     output	: out std_logic);
end component;
 
component latch                  
  port (          
    clk : in  std_logic;
    d   : in  std_logic;
    q   : out std_logic;
    qbar: out std_logic); 
end component;                          



for invertRowAndColEnn, invertWriteEnn : inverter use entity work.inverter(structural);
for nandEn, nandRd : nand2 use entity work.nand2(structural);

for tx_0, tx_1, tx_2, tx_3, tx_4, tx_5, tx_6, tx_7: tx use entity work.tx(structural);
for latch_0, latch_1, latch_2, latch_3, latch_4, latch_5, latch_6, latch_7: latch use entity work.latch(structural);

signal rowAndColEn, rowAndColEnn, writeEn, writeEnn, rdn: std_logic;
signal outputTx: std_logic_vector (7 downto 0);

-- this just stores -Qn which isn't used in this case
signal temp : std_logic;

begin


	nandEn: nand2   port map (rowEn, colEn, rowAndColEnn);
	invertRowAndColEnn:   inverter port map (rowAndColEnn, rowAndColEn);

	nandRd: nand2   port map (rowAndColEn, wr, writeEnn);  
	invertWriteEnn: inverter port map (writeEnn, writeEn);

	-- latch the input in memory

	latch_0: latch port map (writeEn, input(0), outputTx(0), temp);
	latch_1: latch port map (writeEn, input(1), outputTx(1), temp);
	latch_2: latch port map (writeEn, input(2), outputTx(2), temp);
	latch_3: latch port map (writeEn, input(3), outputTx(3), temp);
	latch_4: latch port map (writeEn, input(4), outputTx(4), temp);
	latch_5: latch port map (writeEn, input(5), outputTx(5), temp);
	latch_6: latch port map (writeEn, input(6), outputTx(6), temp);
	latch_7: latch port map (writeEn, input(7), outputTx(7), temp);

	-- the transmission gates will allow transmission if this is the correct block 

	tx_0: tx port map (rowAndColEn, rowAndColEnn, outputTx(0), output(0));  
	tx_1: tx port map (rowAndColEn, rowAndColEnn, outputTx(1), output(1));
	tx_2: tx port map (rowAndColEn, rowAndColEnn, outputTx(2), output(2));
	tx_3: tx port map (rowAndColEn, rowAndColEnn, outputTx(3), output(3));
	tx_4: tx port map (rowAndColEn, rowAndColEnn, outputTx(4), output(4)); 
	tx_5: tx port map (rowAndColEn, rowAndColEnn, outputTx(5), output(5));
	tx_6: tx port map (rowAndColEn, rowAndColEnn, outputTx(6), output(6)); 
	tx_7: tx port map (rowAndColEn, rowAndColEnn, outputTx(7), output(7));

end structural;
