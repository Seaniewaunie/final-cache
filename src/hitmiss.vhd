-- Entity: hitmiss
-- Architecture: structural
-- Author: mousta1
-- Date: 11/08/2017

library IEEE;
use IEEE.std_logic_1164.all;

entity hitmiss is
    port(
        in1    : in  std_logic_vector(2 downto 0);
        in2    : in  std_logic_vector(2 downto 0);
        valid  : in  std_logic;
        hit    : out std_logic );
end hitmiss;

architecture structural of hitmiss is

component and2
     port (
	input1: in std_logic;
	input2: in std_logic;
	output : out std_logic);
end component;

component xnor2
     port (
	input1	: in std_logic;
	input2	: in std_logic;	
	output	: out std_logic);
end component;



for xnor2_1, xnor2_2, xnor2_3: xnor2 use entity work.xnor2(structural);
for and2_1, and2_2, and2_3: and2 use entity work.and2(structural);

signal xnored : std_logic_vector( 2 downto 0);
signal anded : std_logic_vector( 1 downto 0);

begin

-- Using xnors, check all three bits of 1 and 2 and compare them
xnor2_1: xnor2 port map ( in1(0), in2(0), xnored(0) ); 
xnor2_2: xnor2 port map ( in1(1), in2(1), xnored(1) );
xnor2_3: xnor2 port map ( in1(2), in2(2), xnored(2) );

-- and it to see if it's a hit
and2_1: and2 port map ( xnored(0), valid, anded(0) );
and2_2: and2 port map ( xnored(1), anded(0) , anded(1) );
and2_3: and2 port map ( xnored(2), anded(1) , hit );

end structural;
