--
-- Entity: colEn_test 
-- Architecture : vhdl 
-- Author: sgraff2
-- Created On: 11/13/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity colEn_test is

end colEn_test;

architecture test of colEn_test is

component colEn

  port (
    sel	        : in  std_logic_vector(1 downto 0);
    col0	: out  std_logic;
    col1        : out  std_logic;
    col2        : out  std_logic;
    col3	: out  std_logic);
end component;

for c1 : colEn use entity work.colEn(structural);
   signal ip : std_logic_vector(1 downto 0);
   signal cp1, cp2, cp3, cp4 : std_logic;
   signal clock : std_logic;

begin

c1 : colEn port map (ip, cp1, cp2, cp3, cp4);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/colEn_in.txt";
  file outfile : text is out "./test/colEn_out.txt";
  variable ip1 : std_logic_vector(1 downto 0);
  variable cp11, cp22, cp33, cp44 : std_logic;
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,ip1);
    ip<=ip1;

    wait until falling_edge(clock);

    cp11:=cp1;

    write(buf,cp11);
    writeline(outfile,buf);


    cp22:=cp2;

    write(buf,cp22);
    writeline(outfile,buf);

    cp33:=cp3;

    write(buf,cp33);
    writeline(outfile,buf);


    cp44:=cp4;

    write(buf,cp44);
    writeline(outfile,buf);


  end loop;

end process io_process;

end test;
