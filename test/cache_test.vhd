--
-- Entity: cache_test 
-- Architecture : vhdl 
-- Author: sgraff2
-- Created On: 11/15/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity cache_test is

end cache_test;

architecture test of cache_test is

component cache
port (
    inputData   : in  std_logic_vector(7 downto 0);
    offBlock    : in  std_logic_vector(2 downto 0);
    offByte     : in  std_logic_vector(1 downto 0);
    tagIn       : in  std_logic_vector(2 downto 0);
    reset	: in  std_logic;
    clk		: in  std_logic;
    write	: in  std_logic;
    valid	: out std_logic;
    tagOut	: out std_logic_vector(2 downto 0);
    outputData	: out std_logic_vector(7 downto 0));	
end component;

for c1 : cache use entity work.cache(structural);
   signal ip,op : std_logic_vector(7 downto 0);
   signal obl, tip, top : std_logic_vector(2 downto 0);
   signal oby : std_logic_vector(1 downto 0);
   signal rp, wrp, vp : std_logic;
   signal clock : std_logic;

begin

c1 : cache port map (ip, obl, oby, tip, rp, clock, wrp, vp, top, op);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/cache_in.txt";
  file outfile : text is out "./test/cache_out.txt";
  variable ip1,op1 : std_logic_vector(7 downto 0);
  variable obl1, tip1, top1 : std_logic_vector(2 downto 0);
  variable oby1 : std_logic_vector(1 downto 0);
  variable rp1, wrp1, vp1 : std_logic;
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,ip1);
    ip<=ip1;

    readline(infile,buf);
    read (buf,obl1);
    obl<=obl1;

    readline(infile,buf);
    read (buf,oby1);
    oby<=oby1;

    readline(infile,buf);
    read (buf,tip1);
    tip<=tip1;

    readline(infile,buf);
    read (buf,rp1);
    rp<=rp1;

    readline(infile,buf);
    read (buf,wrp1);
    wrp<=wrp1;




    wait until falling_edge(clock);

    vp1:=vp;

    write(buf,vp1);
    writeline(outfile,buf);

    top1:=top;

    write(buf,top1);
    writeline(outfile,buf);

    op1:=op;

    write(buf,op1);
    writeline(outfile,buf);

  end loop;

end process io_process;

end test;
