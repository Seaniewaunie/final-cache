--
-- Entity: memBlock_test 
-- Architecture : vhdl 
-- Author: sgraff2
-- Created On: 11/08/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity memBlock_test is

end memBlock_test;

architecture test of memBlock_test is

component memBlock
  port(
    inputData   : in std_logic_vector(7 downto 0);
    blockEn     : in std_logic;
    blockEnn    : in std_logic;
    byteEn      : in std_logic_vector(3 downto 0);
    inputTag    : in std_logic_vector(2 downto 0);
    rstn        : in std_logic;
    rst		: in std_logic;
    clk		: in std_logic;
    wr    	: in std_logic;
    valid       : out std_logic;
    outputTag      : out std_logic_vector(2 downto 0);
    outputData0  : out std_logic_vector(7 downto 0);    
    outputData1  : out std_logic_vector(7 downto 0);
    outputData2  : out std_logic_vector(7 downto 0);
    outputData3  : out std_logic_vector(7 downto 0));
end component;

for m1 : memBlock use entity work.memBlock(structural);
   signal indata,outdata0,outdata1,outdata2,outdata3 : std_logic_vector(7 downto 0);
   signal byteen : std_logic_vector(3 downto 0);
   signal intag, outtag  :std_logic_vector(2 downto 0);
   signal blocken,blockenn,resetn,reset,writeen,val : std_logic;
   signal clock : std_logic;

begin

m1 : memBlock port map (indata,blocken,blockenn,byteen,intag,resetn,reset,clock,writeen,val,outtag,outdata0,outdata1,outdata2,outdata3);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/memBlock_in.txt";
  file outfile : text is out "./test/memBlock_out.txt";
  
  variable indata1,outdata01,outdata11,outdata21,outdata31: std_logic_vector(7 downto 0);
  variable byteen1 : std_logic_vector(3 downto 0);
  variable intag1, outtag1  :std_logic_vector(2 downto 0);
  variable blocken1,blockenn1,resetn1,reset1,write1,val1 : std_logic;

  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,indata1);
    indata<=indata1;

    readline(infile,buf);
    read (buf,blocken1);
    blocken<=blocken1;
    
    readline(infile,buf);
    read (buf,blockenn1);
    blockenn<=blockenn1;

    readline(infile,buf);
    read (buf,byteen1);
    byteen<=byteen1;
  
    readline(infile,buf);
    read (buf,intag1);
    intag<=intag1;

    readline(infile,buf);
    read (buf,resetn1);
    resetn<=resetn1;
 
    readline(infile,buf);
    read (buf,reset1);
    reset<=reset1;  
 
    readline(infile,buf);
    read (buf,write1);
    writeen<=write1;
    
    readline(infile,buf);
    read (buf,val1);
    val<=val1;


    
    wait until falling_edge(clock);

    outtag1:=outtag;
    write(buf,outtag1);
    writeline(outfile,buf);

    outdata01:=outdata0;
    write(buf,outdata01);
    writeline(outfile,buf);
    
    outdata11:=outdata1;
    write(buf,outdata11);
    writeline(outfile,buf);
    
    outdata21:=outdata2;
    write(buf,outdata21);
    writeline(outfile,buf);
    
    outdata31:=outdata3;
    write(buf,outdata31);
    writeline(outfile,buf);
    


  end loop;

end process io_process;

end test;
