--
-- Entity: stateMachine_test 
-- Architecture : vhdl 
-- Author: sgraff2
-- Created On: 11/16/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity stateMachine_test is

end stateMachine_test;

architecture test of stateMachine_test is

component stateMachine
    port(
        clk     : in std_logic;
        start   : in std_logic;
        hitMiss : in std_logic;
        read    : in std_logic;
        write   : in std_logic;
        memEn   : out std_logic;
        cacheAd : out std_logic_vector(1 downto 0);
        cacheWr : out std_logic;
        CPUoutEn: out std_logic;
        busy    : out std_logic);
end component;


for sm1 : stateMachine use entity work.stateMachine(structural);
   signal startp, hitmissp, readp, writep, memoryEnp, cacheWrp, cpuEnp, busyp : std_logic;
   signal cacheAdp: std_logic_vector(1 downto 0);
   signal clock : std_logic;

begin

sm1 : stateMachine port map (clock, startp, hitmissp, readp, writep, memoryEnp, cacheAdp, cacheWrp, cpuEnp, busyp);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/stateMachine_in.txt";
  file outfile : text is out "./test/stateMachine_out.txt";
  variable startp1, hitmissp1, readp1, writep1, memoryEnp1, cacheWrp1, cpuEnp1, busyp : std_logic;
  variable cacheAdp1: std_logic_vector(1 downto 0);
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,startp1);
    startp<=startp1;

    readline(infile,buf);
    read (buf,hitmissp1);
    hitmissp<=hitmissp1;

    readline(infile,buf);
    read (buf,readp1);
    readp<=readp1;

    readline(infile,buf);
    read (buf,writep1);
    writep<=writep1;


    wait until falling_edge(clock);

    memoryEnp1:=memoryEnp;

    write(buf,memoryEnp1);
    writeline(outfile,buf);

    cacheAdp1:=cacheAdp;

    write(buf,cacheAdp1);
    writeline(outfile,buf);

    cacheWrp1:=cacheWrp;

    write(buf,cacheWrp1);
    writeline(outfile,buf);

    cpuEnp1:=cpuEnp;

    write(buf,cpuEnp1);
    writeline(outfile,buf);

    busyp:=busyp;

    write(buf,busyp);
    writeline(outfile,buf);

  end loop;

end process io_process;

end test;
