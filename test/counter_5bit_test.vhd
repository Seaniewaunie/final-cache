--
-- Entity: counter_5bit_test 
-- Architecture : vhdl 
-- Author: sgraff2
-- Created On: 11/13/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity counter_5bit_test is

end counter_5bit_test;

architecture test of counter_5bit_test is

component counter_5bit                   
  port ( 
     countEn 	 : in  std_logic;
     reset	 : in  std_logic;
     Clk  	 : in std_logic;
     output      : out  std_logic_vector(4 downto 0);
     outputn     : out std_logic_vector(4 downto 0)); 
end component;                          

for c1 : counter_5bit use entity work.counter_5bit(structural);
   signal op,opn : std_logic_vector(4 downto 0);
   signal cp, rp : std_logic;
   signal clock : std_logic;

begin

c1 : counter_5bit port map (cp, rp, clock, op, opn);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/counter_5bit_in.txt";
  file outfile : text is out "./test/counter_5bit_out.txt";
  variable op1,opn1 : std_logic_vector(4 downto 0);
  variable cp1, rp1 : std_logic;
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,cp1);
    cp<=cp1;

    readline(infile,buf);
    read (buf,rp1);
    rp<=rp1;

    wait until falling_edge(clock);

    op1:=op;

    write(buf,op1);
    writeline(outfile,buf);

    opn1:=opn;

    write(buf,opn1);
    writeline(outfile,buf);


  end loop;

end process io_process;

end test;
