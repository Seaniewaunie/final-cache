--
-- Entity: valid_tag_test 
-- Architecture : vhdl 
-- Author: sgraff2
-- Created On: 11/10/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity valid_tag_test is

end valid_tag_test;

architecture test of valid_tag_test is

component valid_tag
  port(
    blockEn     : in std_logic;
    blockEnn    : in std_logic;
    inputTag    : in std_logic_vector(2 downto 0);
    rstn        : in std_logic;
    rst		: in std_logic;
    clk		: in std_logic;
    wr    	: in std_logic;
    valid       : out std_logic;
    outputTag      : out std_logic_vector(2 downto 0));
end component;

for vt1 : valid_tag use entity work.valid_tag(structural);
   signal blockenp,blockennp : std_logic;
   signal intagp, outtagp : std_logic_vector(2 downto 0);
   signal rstnp, rstp, wrp, validp : std_logic;
   signal clock : std_logic;

begin

vt1 : valid_tag port map (blockenp, blockennp, intagp, rstnp, rstp, clock, wrp, validp, outtagp);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/valid_tag_in.txt";
  file outfile : text is out "./test/valid_tag_out.txt";
  variable blockenp1,blockennp1 : std_logic; 
  variable intagp1, outtagp1 : std_logic_vector(2 downto 0);
  variable rstnp1, rstp1, wrp1, validp1 : std_logic;
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,blockenp1);
    blockenp<=blockenp1;

    readline(infile,buf);
    read (buf,blockennp1);
    blockennp<=blockennp1;

    readline(infile,buf);
    read (buf,intagp1);
    intagp<=intagp1;

    readline(infile,buf);
    read (buf,rstnp1);
    rstnp<=rstnp1;

    readline(infile,buf);
    read (buf,rstp1);
    rstp<=rstp1;

    readline(infile,buf);
    read (buf,wrp1);
    wrp<=wrp1;




    wait until falling_edge(clock);

    validp1:=validp;

    write(buf,validp1);
    writeline(outfile,buf);

    outtagp1:=outtagp;

    write(buf,outtagp1);
    writeline(outfile,buf);


  end loop;

end process io_process;

end test;
