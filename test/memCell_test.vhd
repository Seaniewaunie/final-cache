--
-- Entity: memCell_test 
-- Architecture : vhdl 
-- Author: sgraff2
-- Created On: 11/08/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity memCell_test is

end memCell_test;

architecture test of memCell_test is

component memCell

  port (
    input   : in  std_logic_vector(7 downto 0);	
    rowEn	: in  std_logic; 
    colEn	: in  std_logic; 
    wr   	: in  std_logic;				
    output  : out std_logic_vector(7 downto 0));

end component;

for m1 : memCell use entity work.memCell(structural);
   signal ip,op : std_logic_vector(7 downto 0);
   signal ren, cen, wri : std_logic;
   signal clock : std_logic;

begin

m1 : memCell port map (ip, ren, cen, wri, op);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/memCell_in.txt";
  file outfile : text is out "./test/memCell_out.txt";
  variable ip1,op1 : std_logic_vector(7 downto 0);
  variable ren1, cen1, write1 : std_logic;
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,ip1);
    ip<=ip1;

    readline(infile,buf);
    read (buf,ren1);
    ren<=ren1;

    readline(infile,buf);
    read (buf,cen1);
    cen<=cen1;

    readline(infile,buf);
    read (buf,write1);
    wri<=write1;


    wait until falling_edge(clock);

    op1:=op;

    write(buf,op1);
    writeline(outfile,buf);

  end loop;

end process io_process;

end test;
