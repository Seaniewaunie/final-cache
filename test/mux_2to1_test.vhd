--
-- Entity: mux_2to1_test 
-- Architecture : vhdl 
-- Author: sgraff2
-- Created On: 11/10/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity mux_2to1_test is

end mux_2to1_test;

architecture test of mux_2to1_test is

component mux_2to1
    port(
    sel	:	in std_logic;
    in0  :	in std_logic_vector(1 downto 0);
    in1  :	in std_logic_vector(1 downto 0);
    output:	out std_logic_vector(1 downto 0));
end component;   

for m1 : mux_2to1 use entity work.mux_2to1(structural);
   signal selp : std_logic;
   signal ip0, ip1, op : std_logic_vector(1 downto 0);
   signal clock : std_logic;

begin

m1 : mux_2to1 port map (selp, ip0, ip1, op);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/mux_2to1_in.txt";
  file outfile : text is out "./test/mux_2to1_out.txt";
  variable selp1: std_logic;
  variable ip00,ip11,op1 : std_logic_vector(1 downto 0);
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,selp1);
    selp<=selp1;

    readline(infile,buf);
    read (buf,ip00);
    ip0<=ip00;

    readline(infile,buf);
    read (buf,ip11);
    ip1<=ip11;

    wait until falling_edge(clock);

    op1:=op;

    write(buf,op1);
    writeline(outfile,buf);

  end loop;

end process io_process;

end test;
