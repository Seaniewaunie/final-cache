--
-- Entity: latch_test 
-- Architecture : vhdl 
-- Author: sgraff2
-- Created On: 11/08/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity latch_test is

end latch_test;

architecture test of latch_test is

component latch                     
  port (          
        clk : in  std_logic;
        d   : in  std_logic;
        q   : out std_logic;
        qbar: out std_logic); 
end component;                          

for l1 : latch use entity work.latch(structural);
   signal ip, op, opn : std_logic;
   signal clock : std_logic;

begin

l1 : latch port map (clock, ip, op, opn);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/latch_in.txt";
  file outfile : text is out "./test/latch_out.txt";
  variable cp1, ip1, op1, opn1 : std_logic;
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,ip1);
    ip<=ip1;


    wait until falling_edge(clock);

    op1:=op;

    write(buf,op1);
    writeline(outfile,buf);

    opn1:=opn;

    write(buf,opn1);
    writeline(outfile,buf);



  end loop;

end process io_process;

end test;
