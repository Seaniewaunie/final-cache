--
-- Entity: rowEn_test 
-- Architecture : vhdl 
-- Author: mousta1
-- Created On: 11/08/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity rowEn_test is

end rowEn_test;

architecture test of rowEn_test is

component rowEn

  port (
    sel	  : in  std_logic_vector(2 downto 0);
    row0  : out  std_logic;
    row1  : out  std_logic;
    row2  : out  std_logic;
    row3  : out  std_logic;
    row4  : out  std_logic;
    row5  : out  std_logic;
    row6  : out  std_logic;
    row7  : out  std_logic);
end component;

for r1 : rowEn use entity work.rowEn(structural);
   signal ip : std_logic_vector(2 downto 0);
   signal op0, op1, op2, op3, op4, op5, op6, op7 : std_logic;
   signal clock : std_logic;

begin

r1 : rowEn port map (ip, op0, op1, op2, op3, op4, op5, op6, op7);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/rowEn_in.txt";
  file outfile : text is out "./test/rowEn_out.txt";
  variable ip1 : std_logic_vector(2 downto 0);
  variable op00, op01, op02, op03, op04, op05, op06, op07 : std_logic; 
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,ip1);
    ip<=ip1;

    wait until falling_edge(clock);

    op00:=op0;

    write(buf,op00);
    writeline(outfile,buf);

    op01:=op1;

    write(buf,op01);
    writeline(outfile,buf);


    op02:=op2;

    write(buf,op02);
    writeline(outfile,buf);


    op03:=op3;

    write(buf,op03);
    writeline(outfile,buf);


    op04:=op4;

    write(buf,op04);
    writeline(outfile,buf);


    op05:=op5;

    write(buf,op05);
    writeline(outfile,buf);


    op06:=op6;

    write(buf,op06);
    writeline(outfile,buf);

    op07:=op7;

    write(buf,op07);
    writeline(outfile,buf);



  end loop;

end process io_process;

end test;
