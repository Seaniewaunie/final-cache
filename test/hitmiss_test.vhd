--
-- Entity: hitmiss_test 
-- Architecture : vhdl 
-- Author: mousta1
-- Created On: 11/13/17
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity hitmiss_test is

end hitmiss_test;

architecture test of hitmiss_test is

component hitmiss
    port(
        in1    : in  std_logic_vector(2 downto 0);
        in2    : in  std_logic_vector(2 downto 0);
        valid  : in  std_logic;
        hit    : out std_logic );
end component;

for hm1 : hitmiss use entity work.hitmiss(structural);
   signal ip, ipp : std_logic_vector(2 downto 0);
   signal vp, hp : std_logic;
   signal clock : std_logic;

begin

hm1 : hitmiss port map (ip, ipp, vp, hp);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;


io_process: process

  file infile  : text is in "./test/hitmiss_in.txt";
  file outfile : text is out "./test/hitmiss_out.txt";
  variable ip1,ipp1 : std_logic_vector(2 downto 0);
  variable vp1, hp1 : std_logic;
  variable buf : line;

begin

  while not (endfile(infile)) loop

    readline(infile,buf);
    read (buf,ip1);
    ip<=ip1;

    readline(infile,buf);
    read (buf,ipp1);
    ipp<=ipp1;

    readline(infile,buf);
    read (buf,vp1);
    vp<=vp1;

    wait until falling_edge(clock);

    hp1:=hp;

    write(buf,hp1);
    writeline(outfile,buf);

  end loop;

end process io_process;

end test;
