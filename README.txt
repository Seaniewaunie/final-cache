File system structure:
    src/ contains the vhdl code for each part
    test/ contains the vhdl tests
    images/ contains the waveform pdfs

NOTE: we assume you have all the "tools" (ie ncsim... cadence... etc, in the ../ directory from this current one.

Usage: 
    make TEST=<module>_test

Waveforms:

    make TEST=<module>_test INPUT=-gui

Note: <module> is any part of the project... ie "chip" or "cache", etc.

Example:

    make TEST=chip_test INPUT=-gui

    This command would run the chip test and open the wave simulation program.

After running any test, and if you don't use the INPUT=-gui part, then you can view the the test/ directory for the <module>_out.txt file.

Also, when the test runs, usually it won't exit for you. Just do ctrl+C and exit, the output files will be in the test/ directory.
